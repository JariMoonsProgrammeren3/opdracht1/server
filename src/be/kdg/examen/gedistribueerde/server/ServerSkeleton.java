package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.client.DocumentImpl;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class ServerSkeleton {
    private final MessageManager messageManager;
    private final Server server;

    public ServerSkeleton() {
        messageManager = new MessageManager();
        server = new ServerImpl();
    }

    public void handleLog(MethodCallMessage request){
        Document document = new DocumentImpl(request.getParameter("document"));
        server.log(document);

        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "messageResult");
        reply.setParameter("messageResult", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void handleRequest(MethodCallMessage request) {
        String methodName = request.getMethodName();
        if ("log".equals(methodName)) {
            handleLog(request);
        } else if ("create".equals(methodName)){
            handleCreate(request);
        }else if ("toUpper".equals(methodName)){
            handleToUpper(request);
        }else if ("toLower".equals(methodName)){
            handleToLower(request);
        }else if ("type".equals(methodName)){
            handleType(request);
        }
    }

    private void handleCreate(MethodCallMessage request) {
        Document document = server.create(request.getParameter("string"));
        sendReply(document,request);
    }

    private void handleToUpper(MethodCallMessage request){
        Document document = new DocumentImpl(request.getParameter("document"));
        server.toUpper(document);
        sendReply(document,request);
    }

    private void handleToLower(MethodCallMessage request){
        Document document = new DocumentImpl(request.getParameter("document"));
        server.toLower(document);
        sendReply(document,request);
    }

    private void handleType(MethodCallMessage request){
        Document document = new DocumentImpl(request.getParameter("document"));
        server.type(document,request.getParameter("newText"));
        sendReply(document,request);
    }

    private void sendReply(Document document, MethodCallMessage request) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),"reply");
        message.setParameter("text",document.getText());
        messageManager.send(message,request.getOriginator());
    }

    public void run() {
        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }
}
