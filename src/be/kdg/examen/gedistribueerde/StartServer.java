package be.kdg.examen.gedistribueerde;

import be.kdg.examen.gedistribueerde.server.ServerSkeleton;

public class StartServer {
    public static void main(String[] args) {
        ServerSkeleton serverSkeleton = new ServerSkeleton();
        serverSkeleton.run();
    }
}
